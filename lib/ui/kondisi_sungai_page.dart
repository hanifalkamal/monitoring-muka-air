import 'package:flutter/material.dart';

class KondisiSungaiPage extends StatelessWidget {
  final String statusHilir;
  final String statusHulu;
  // const KondisiSungaiPage({Key? key}) : super(key: key);
  KondisiSungaiPage(final this.statusHilir, final this.statusHulu);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 40, left: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Kondisi Sungai ", textAlign: TextAlign.left),
          Container(
              margin: EdgeInsets.only(right: 5),
              // width: double.infinity,
              child: Text(SetStatusSungai(statusHilir, statusHulu))),
        ],
      ),
    );
  }

  String SetStatusSungai(final String statusHilir, final String statusHulu){

    if (statusHilir == "Tinggi" && statusHulu == "Tinggi"){
      return "Awas";
    } else if (statusHilir == "Tinggi" && statusHulu == "Sedang"){
      return "Siaga";
    } else if (statusHilir == "Tinggi" && statusHulu == "Rendah"){
      return "Siaga";
    } else if (statusHilir == "Sedang" && statusHulu == "Tinggi"){
      return "Siaga";
    } else if (statusHilir == "Sedang" && statusHulu == "Sedang"){
      return "Waspada";
    } else if (statusHilir == "Sedang" && statusHulu == "Rendah"){
      return "Waspada";
    } else if (statusHilir == "Rendah" && statusHulu == "Tinggi"){
      return "Waspada";
    } else if (statusHilir == "Rendah" && statusHulu == "Sedang"){
      return "Normal";
    } else if (statusHilir == "Rendah" && statusHulu == "Rendah"){
      return "Normal";
    } else {
      return "N/A";
    }


  }
}
