import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:monitor_muka_air/bloc/data_kedalaman_bloc.dart';
import 'package:monitor_muka_air/bloc/sungai_bloc.dart';
import 'package:monitor_muka_air/bloc/sungai_event.dart';
import 'package:monitor_muka_air/bloc/sungai_state.dart';
import 'package:monitor_muka_air/model/data_kedalaman.dart';
import 'package:monitor_muka_air/util/calculate.dart' as calculate;

// ignore: must_be_immutable
int kedalaman = 0;

class KedalamanSungaiPage extends StatelessWidget {
  final String kedalaman;
  KedalamanSungaiPage(this.kedalaman);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20, left: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Kedalaman Sungai (simulator) ", textAlign: TextAlign.left),
          Container(
            margin: EdgeInsets.only(right: 5),
            child: Text(kedalaman + " m"),
          ),
        ],
      ),
    );
  }

  // Widget listenToBloc() {
  //   //  kedalaman;
  //   return BlocListener<SungaiBloc, SungaiState>(
  //       listener: (context, state) {
  //         if (state is SungaiBlocState) {
  //           kedalaman = int.parse(state.value.kedalaman);
  //           // calculate.calculateMukaAir(jarak, kedalaman);
  //           // print("kedalaman = $kedalaman");
  //         }
  //       },
  //       child: Text(
  //         "$kedalaman m",
  //       ));
  // }
}
