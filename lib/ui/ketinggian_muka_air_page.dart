import 'package:flutter/material.dart';

class KetinggianMukaAir extends StatelessWidget {
  final String ketinggianMukaAir;

  KetinggianMukaAir(this.ketinggianMukaAir);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20, left: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Ketinggian Muka Air ", textAlign: TextAlign.left),
          Container(
              margin: EdgeInsets.only(right: 5),
              // width: double.infinity,
              child: Text("$ketinggianMukaAir cm")),
        ],
      ),
    );
  }
}
