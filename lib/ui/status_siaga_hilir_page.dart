import 'package:flutter/material.dart';
import 'package:monitor_muka_air/ui/main_page.dart';


class StatusSiagaHilirPage extends StatelessWidget {
  final String ketinggianMukaAir;
  final String kedalamanSungai;
  // const StatusSiagaHuluPage({Key? key}) : super(key: key);
  StatusSiagaHilirPage(final this.ketinggianMukaAir, final this.kedalamanSungai);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20, left: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Status Siaga ", textAlign: TextAlign.left),
          Container(
              margin: EdgeInsets.only(right: 5),
              // width: double.infinity,
              child: Text(SetStatusSiaga(ketinggianMukaAir, kedalamanSungai))),
        ],
      ),
    );
  }
}


String SetStatusSiaga(final String ketinggianMukaAir, final String kedalamanSungai){
  var doubleKetMukaAir = double.parse(ketinggianMukaAir);
  var doubleKedalaman = double.parse(kedalamanSungai);
  var tinggi = (doubleKedalaman * 100) * (80 / 100);
  var sedang = (doubleKedalaman * 100) * (40 / 100);

  if (doubleKetMukaAir >= tinggi){
    statusSiagaHilir = "Tinggi";
    return "Tinggi";
  } else if (doubleKetMukaAir < tinggi && doubleKetMukaAir >= sedang){
    statusSiagaHilir = "Sedang";
    return "Sedang";
  } else {
    statusSiagaHilir = "Rendah";
    return "Rendah";
  }

}

