import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:monitor_muka_air/bloc/sungai_bloc.dart';
import 'package:monitor_muka_air/bloc/sungai_event.dart';
import 'package:monitor_muka_air/bloc/sungai_state.dart';
import 'package:monitor_muka_air/model/data_jarak.dart';
import 'package:monitor_muka_air/model/data_kedalaman.dart';
import 'package:monitor_muka_air/ui/kedalaman_sungai_page.dart';
import 'package:monitor_muka_air/ui/ketinggian_muka_air_page.dart';
import 'package:monitor_muka_air/ui/kondisi_sungai_page.dart';
import 'package:monitor_muka_air/ui/status_siaga_hilir_page.dart';
import 'package:monitor_muka_air/ui/status_siaga_hulu_page.dart';
import 'package:monitor_muka_air/util/calculate.dart' as calculate;
import 'package:monitor_muka_air/util/notif/flutter_local_notifications.dart';
import 'package:monitor_muka_air/util/notif/flutter_local_notifications_plugin.dart';
import 'package:monitor_muka_air/util/notif/notif.dart' as sendNotif;
import 'package:onesignal_flutter/onesignal_flutter.dart';

// int jarak = 0;
String tinggiMukaAir = "0";
String kedalaman = "0";
String jarak = "0";
String statusSiaga = "";

String tinggiMukaAirHilir = "0";
String kedalamanHilir = "0";
String jarakHilir = "0";
String statusSiagaHilir = "";

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  Timer? timer;

  @override
  void initState() {
    super.initState();

    OneSignal.shared.setNotificationWillShowInForegroundHandler(
        (OSNotificationReceivedEvent event) {
      setState(() {});
      event.complete(event.notification);
    });

    timer = Timer.periodic(Duration(seconds: 1), (Timer t) {
      SungaiBloc bloc = BlocProvider.of<SungaiBloc>(context);
      bloc.add(SungaiBlocEvent("Sungai Cicaheum"));
      setState(() {});
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //

    return Scaffold(
        appBar: AppBar(
          title: Text("MONITORING MUKA AIR"),
        ),
        body: Container(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 20),
                child: Text(
                  "Persamaan Ketinggian Muka Air",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 20, left: 5),
                child: Text(
                  "Sungai Hulu : ",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.left,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 20, left: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Jarak Sensor ke permukaan air sungai ",
                            textAlign: TextAlign.left),
                        Container(
                            margin: EdgeInsets.only(right: 5),
                            // width: double.infinity,
                            child: BlocListener<SungaiBloc, SungaiState>(
                              listener: (context, state) {
                                if (state is SungaiBlocState) {
                                  jarak = state.value.jarak;
                                  kedalaman = state.value.kedalaman;
                                  tinggiMukaAir = (state.value.tinggiMukaAir);
                                  // _showNotification();
                                }
                                // bloc.add(
                                //     SungaiBlocKedalaman("Sungai Cicaheum"));
                              },
                              child: Text("$jarak cm"),
                            )),
                      ],
                    ),
                  ),
                  BlocProvider(
                      create: (context) => SungaiBloc(),
                      child: KedalamanSungaiPage(kedalaman)),
                  BlocProvider(
                      create: (context) => SungaiBloc(),
                      child: KetinggianMukaAir(tinggiMukaAir)),
                  BlocProvider(
                      create: (context) => SungaiBloc(),
                      child: StatusSiagaHuluPage(tinggiMukaAir, kedalaman)
                  ),
                ],
              ),
              Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 30, left: 5),
                child: Text(
                  "Sungai Hilir : ",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.left,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 20, left: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Jarak Sensor ke permukaan air sungai ",
                            textAlign: TextAlign.left),
                        Container(
                            margin: EdgeInsets.only(right: 5),
                            // width: double.infinity,
                            child: BlocListener<SungaiBloc, SungaiState>(
                              listener: (context, state) {
                                if (state is SungaiBlocState) {
                                  jarakHilir = state.value.jarak;
                                  kedalamanHilir = state.value.kedalaman;
                                  tinggiMukaAirHilir = (state.value.tinggiMukaAir);

                                  // _showNotification();
                                }
                                // bloc.add(
                                //     SungaiBlocKedalaman("Sungai Cicaheum"));
                              },
                              child: Text("$jarakHilir cm"),
                            )),
                      ],
                    ),
                  ),
                  BlocProvider(
                      create: (context) => SungaiBloc(),
                      child: KedalamanSungaiPage(kedalamanHilir)),
                  BlocProvider(
                      create: (context) => SungaiBloc(),
                      child: KetinggianMukaAir(tinggiMukaAirHilir)),
                  BlocProvider(
                      create: (context) => SungaiBloc(),
                      child: StatusSiagaHilirPage(tinggiMukaAirHilir, kedalamanHilir)),
                  BlocProvider(
                      create: (context) => SungaiBloc(),
                      child: KondisiSungaiPage(statusSiaga, statusSiagaHilir)),
                ],
              )
            ],
          ),
        ));
  }
}
