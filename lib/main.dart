import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:monitor_muka_air/bloc/data_jarak_bloc.dart';
import 'package:monitor_muka_air/bloc/sungai_bloc.dart';
import 'package:monitor_muka_air/model/data_jarak.dart';
import 'package:monitor_muka_air/ui/main_page.dart';
import 'package:monitor_muka_air/util/global.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  OneSignal.shared.setAppId("efbc4bab-1f38-4966-ba68-760de90b3fcf");
  // OneSignal.shared
  //     .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
  //   print("### USER ID ONE SIGNAL = " + changes.to.userId.toString());
  //   Global.OneSignalUserId = changes.to.userId.toString();
  // });
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocProvider(
        create: (context) => SungaiBloc(),
        child: MainPage(),
      ),
    );
  }
}
