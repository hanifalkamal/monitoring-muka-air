import 'package:monitor_muka_air/util/constant.dart';

class Notif {
  List<String>? include_player_ids;
  String? app_id;
  Map<String, dynamic>? contents;
  Map<String, dynamic>? headings;
  String? id;
  int? receipient;
  String? external_id;

  Notif({this.include_player_ids, this.app_id, this.contents, this.headings});
  Notif.response({this.id, this.receipient, this.external_id});

  Map<String, dynamic> toJson() {
    return {
      'include_player_ids': include_player_ids,
      'app_id': app_id,
      'contents': contents,
      'headings': headings
    };
  }

  factory Notif.createResponse(Map<String, dynamic> json) {
    return Notif.response(
        id: json["id"],
        receipient: json["receipient"],
        external_id: json["external_id"]);
  }

  static Map<String, dynamic> contentEn(String text) => {'en': text};
}
