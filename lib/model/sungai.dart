class Sungai {
  bool success = false;
  late String jarak;
  late String tanggalpukul;
  late String kedalaman;
  late String tinggiMukaAir;

  Sungai({
    required this.success,
    required this.jarak,
    required this.tanggalpukul,
    required this.kedalaman,
    required this.tinggiMukaAir,
  });

  Sungai.getJarak({
    required this.success,
    required this.jarak,
    required this.tanggalpukul,
  });

  Sungai.getKedalaman({required this.success, required this.kedalaman});

  factory Sungai.createDataJarak(Map<String, dynamic> object) {
    return Sungai.getJarak(
      success: object["success"],
      jarak: object["jarak"],
      tanggalpukul: object["tanggalpukul"],
    );
  }

  factory Sungai.createDataKedalaman(Map<String, dynamic> object) {
    return Sungai.getKedalaman(
      success: object['success'],
      kedalaman: object['kedalaman'],
    );
  }
}
