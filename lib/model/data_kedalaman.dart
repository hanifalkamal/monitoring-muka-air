import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:monitor_muka_air/util/constant.dart';

class DataKedalaman {
  bool? success;
  String? kedalaman;

  DataKedalaman({
    this.success,
    this.kedalaman,
  });

  Map<String, dynamic> toMap() {
    return {
      'success': success,
      'kedalaman': kedalaman,
    };
  }

  factory DataKedalaman.fromMap(Map<String, dynamic>? map) {
    if (map == null)
      return DataKedalaman(
        success: false,
        kedalaman: "0",
      );

    return DataKedalaman(
      success: map['success'],
      kedalaman: map['kedalaman'],
    );
  }

  static Future<DataKedalaman> getKedalaman(String namaSungai) async {
    Uri apiUrl = Uri.parse(Constant.Url + Constant.KedalamanPath);
    var map = new Map<String, dynamic>();
    map['nama_sungai'] = namaSungai;

    var apiResult = await http.post(
      apiUrl,
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: map,
    );

    var jsonObject = json.decode(apiResult.body);
    print(apiUrl);
    print(apiResult.body);
    return DataKedalaman.fromMap(jsonObject);
  }

  String toJson() => json.encode(toMap());

  factory DataKedalaman.fromJson(String source) =>
      DataKedalaman.fromMap(json.decode(source));
}

class UninitializedDataKedalaman extends DataKedalaman {}
