import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:monitor_muka_air/util/constant.dart';

class DataJarak {
  bool? success = false;
  String? jarak;
  String? tanggalpukul;

  DataJarak({
    required this.success,
    required this.jarak,
    required this.tanggalpukul,
  });

  factory DataJarak.createDataJarak(Map<String, dynamic> object) {
    return DataJarak(
      success: object["success"],
      jarak: object["jarak"],
      tanggalpukul: object["tanggalpukul"],
    );
  }

  static Future<DataJarak> getDataJarak(String namaSungai) async {
    try {
      Uri apiUrl = Uri.parse("193.168.195.139" + Constant.JarakPath);
      var map = new Map<String, dynamic>();
      print("x");
      map['nama_sungai'] = namaSungai;
      final apiResult = await http.post(
        Uri.http("193.168.195.139", "/beni/get_jarak.php"),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: map,
      );
      print(apiResult.statusCode);
      if (apiResult.statusCode == 200) {
        var jsonObject = json.decode(apiResult.body);
        print(apiUrl);
        print(apiResult.body);
        return DataJarak.createDataJarak(jsonObject);
      } else {
        throw Exception("FAILED");
      }
    } catch (E) {
      print(E.toString());
      throw Exception("FAILED");
    }
  }
}

class UninitializedDataJarak extends DataJarak {
  UninitializedDataJarak()
      : super(success: false, jarak: "0", tanggalpukul: "");
}
