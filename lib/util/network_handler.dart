import 'dart:convert';

import 'package:http/http.dart' as http;

import 'constant.dart';

class NetworkHandler {
  static dynamic connect(String route, Map<String, dynamic> map) async {
    try {
      Uri apiUrl = Uri.parse(Constant.Url + route);
      // print(apiUrl);
      var apiResult = await http.post(
        apiUrl,
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: map,
      );

      if (apiResult.statusCode == 200) {
        var jsonObject = json.decode(apiResult.body);
        // print(apiResult.body);
        return (jsonObject);
      } else {
        throw Exception("FAILED -> " + apiResult.statusCode.toString());
      }
    } catch (_) {
      print("FAIL --> ${_.toString()}");
      throw Exception("FAILED");
    }
  }

  static dynamic createNotif(Map<String, dynamic> map) async {
    try {
      Uri apiUrl = Uri.parse(Constant.OneSignalUrl);
      print(apiUrl);
      print(Constant.OneSignalServerKey);
      print(map.toString());
      var apiResult = await http.post(
        apiUrl,
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': Constant.OneSignalServerKey
        },
        body: map,
      );
      print("POST");
      if (apiResult.statusCode == 200) {
        var jsonObject = json.decode(apiResult.body);
        print(apiResult.body);
        return (jsonObject);
      } else {
        throw Exception("FAILED -> " + apiResult.statusCode.toString());
      }
    } catch (_) {
      print("FAIL --> ${_.toString()}");
      throw Exception("FAILED");
    }
  }
}
