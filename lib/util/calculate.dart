// ignore: library_names
library monitor_muka_air.Calculate;

import 'package:monitor_muka_air/util/global.dart';

// class Calculate {
int? tinggiMukaAir;

double reciprocal(double d) => 1 / d;

double calculateMukaAir(int jarak, double kedalaman) {
  return kedalaman * 100 - jarak.toDouble();
}

String statusSiaga(int kedalaman, int ketinggianMukaAir) {
  if (ketinggianMukaAir < ((30 / 100) * kedalaman)) {
    return "Siaga I";
  } else if (ketinggianMukaAir < ((70 / 100) * kedalaman)) {
    return "Siaga II";
  } else {
    return "Siaga III";
  }
}
// }
