import 'package:monitor_muka_air/model/notif.dart';
import 'package:monitor_muka_air/util/network_handler.dart';

class NotifRepository {
  static Future<Notif> sendNotif(Map<String, dynamic> json) async {
    print("SEND NOTIF");
    var response = await NetworkHandler.createNotif(json);
    print("wait response");
    return Notif.createResponse(json);
  }
}
