import 'dart:convert';

import 'package:monitor_muka_air/model/sungai.dart';
import 'package:monitor_muka_air/util/constant.dart';
import 'package:monitor_muka_air/util/network_handler.dart';
import 'package:http/http.dart' as http;

class SungaiRepository {
  static Future<Sungai> getKedalaman(String namaSungai) async {
    var map = new Map<String, dynamic>();
    map['nama_sungai'] = namaSungai;
    var response = await NetworkHandler.connect(Constant.KedalamanPath, map);
    // var jsonObject = json.decode(response);
    return Sungai.createDataKedalaman(response);
  }

  static Future<Sungai> getJarak(String namaSungai) async {
    // var response = await NetworkHandler.connect(Constant.JarakPath, map);

    var map = new Map<String, dynamic>();
    map['nama_sungai'] = namaSungai;
    var response = await NetworkHandler.connect(Constant.JarakPath, map);
    // var jsonObject = json.decode(response);
    return Sungai.createDataJarak(response);
  }
}
