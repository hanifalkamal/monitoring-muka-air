import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:monitor_muka_air/bloc/sungai_event.dart';
import 'package:monitor_muka_air/bloc/sungai_state.dart';
import 'package:monitor_muka_air/model/notif.dart';
import 'package:monitor_muka_air/model/sungai.dart';
import 'package:monitor_muka_air/repository/notif_repository.dart';
import 'package:monitor_muka_air/repository/sungai_repository.dart';
import 'package:monitor_muka_air/util/calculate.dart';
import 'package:monitor_muka_air/util/constant.dart';
import 'package:monitor_muka_air/util/global.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';

class SungaiBloc extends Bloc<SungaiEvent, SungaiState> {
  SungaiBloc() : super(SungaiInitial());
  Timer? timer;

  @override
  Stream<SungaiState> mapEventToState(SungaiEvent event) async* {
    // ignore: unnecessary_statements
    try {
      late Sungai sungai;
      Sungai defaultSungai = new Sungai(
          success: false,
          jarak: "0",
          tanggalpukul: "",
          kedalaman: "0",
          tinggiMukaAir: "0");

      if (event is SungaiBlocEvent) {
        // print((event as SungaiBlocJarak).value);

        Sungai getJarak =
            await SungaiRepository.getJarak((event as SungaiBlocEvent).value);
        Sungai getKedalaman = await SungaiRepository.getKedalaman(
            (event as SungaiBlocEvent).value);

        int jarak = int.parse(getJarak.jarak);
        double kedalaman = double.parse(getKedalaman.kedalaman);
        // print(jarak);
        // print(kedalaman);
        double ketinggiaMukaAir = calculateMukaAir(jarak, kedalaman);
        // print(ketinggiaMukaAir);
        // String siaga = statusSiaga(kedalaman, ketinggiaMukaAir);
        // print(siaga);
        // print("GLOBAL = " + Global.StatusSiaga);
        // if (siaga != (Global.StatusSiaga)) {
        //   Global.StatusSiaga = siaga;
        //   print("1");
        //   print(Constant.OneSignalUserId);
        //   print(Constant.OneSignalAppId);
        //   List<String> list = [Constant.OneSignalAppId];
        //
        //   Notif notif = new Notif(
        //       include_player_ids: list,
        //       app_id: Constant.OneSignalAppId,
        //       contents: {
        //         'en':
        //             '$siaga, Ketinggian muka air = ${ketinggiaMukaAir.toString()} cm'
        //       },
        //       headings: {
        //         'en': 'Peringatan'
        //       });
        //   print("2");
        //   print(notif.contents.toString());
        //   print(notif.headings.toString());
        //   //Kirim Notif
        //
        //   Notif response = await NotifRepository.sendNotif(notif.toJson());
        //   print("3");
        // }

        Sungai sungai = new Sungai(
            success: true,
            jarak: getJarak.jarak,
            tanggalpukul: "",
            kedalaman: getKedalaman.kedalaman,
            tinggiMukaAir: ketinggiaMukaAir.toString());

        yield (state is SungaiInitial)
            ? SungaiBlocState(defaultSungai)
            : SungaiBlocState(sungai);
      }

      if (event is SungaiBlocKedalaman) {
        sungai = await SungaiRepository.getKedalaman(
            (event as SungaiBlocKedalaman).value);

        yield (state is SungaiInitial)
            ? SungaiKedalamanState(defaultSungai)
            : SungaiKedalamanState(sungai);
      }

      if (event is SungaiBlocCalculateKetinggian) {
        int jarak = (event as SungaiBlocCalculateKetinggian).valueJarak;
        double kedalaman = (event as SungaiBlocCalculateKetinggian).valueKedalaman;
        double ketinggiaMukaAir = calculateMukaAir(jarak, kedalaman);

        sungai = new Sungai(
            success: true,
            jarak: jarak.toString(),
            tanggalpukul: "",
            kedalaman: kedalaman.toString(),
            tinggiMukaAir: tinggiMukaAir.toString());

        yield (state is SungaiInitial)
            ? SungaiCalculateKetinggianState(defaultSungai)
            : SungaiCalculateKetinggianState(sungai);
      }
    } catch (e) {
      print(e.toString());
      throw Exception("FAIL");
    }
  }
}
