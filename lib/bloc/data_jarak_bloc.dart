import 'dart:async';

import 'package:monitor_muka_air/model/data_jarak.dart';
import 'package:bloc/bloc.dart';

class DataJarakBloc extends Bloc<String, DataJarak> {
  late DataJarak dataJarak;

  DataJarakBloc() : super(UninitializedDataJarak());

  // @override
  // DataJarak get initialState => UninitializedDataJarak();

  @override
  Stream<DataJarak> mapEventToState(String event) async* {
    try {
      dataJarak = await DataJarak.getDataJarak(event);
      yield (state is UninitializedDataJarak)
          ? new DataJarak(success: false, jarak: "0", tanggalpukul: "")
          : dataJarak;
    } catch (_) {}
  }
}
