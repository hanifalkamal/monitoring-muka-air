import 'dart:async';

import 'package:monitor_muka_air/model/data_kedalaman.dart';
import 'package:bloc/bloc.dart';

class DataKedalamanBloc extends Bloc<String, DataKedalaman> {
  late DataKedalaman dataKedalaman;

  DataKedalamanBloc() : super(UninitializedDataKedalaman());

  // @override
  // DataKedalaman get initialState => UninitializedDataKedalaman();

  @override
  Stream<DataKedalaman> mapEventToState(String event) async* {
    try {
      // if (state is UninitializedDataKedalaman)
      // if (event.isEmpty) {
      dataKedalaman = await DataKedalaman.getKedalaman(event);
      yield (state is UninitializedDataKedalaman)
          ? new DataKedalaman()
          : dataKedalaman;
      // }
    } catch (_) {}
  }
}
