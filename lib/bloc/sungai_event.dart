import 'package:flutter/material.dart';

@immutable
abstract class SungaiEvent {}

class SungaiBlocEvent extends SungaiEvent {
  final String value;

  SungaiBlocEvent(this.value);
}

class SungaiBlocJarak extends SungaiEvent {
  final String value;

  SungaiBlocJarak(this.value);
}

class SungaiBlocKedalaman extends SungaiEvent {
  final String value;

  SungaiBlocKedalaman(this.value);
}

class SungaiBlocCalculateKetinggian extends SungaiEvent {
  final int valueJarak;
  final double valueKedalaman;

  SungaiBlocCalculateKetinggian(this.valueJarak, this.valueKedalaman);
}
