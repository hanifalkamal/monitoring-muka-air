import 'package:flutter/material.dart';
import 'package:monitor_muka_air/model/sungai.dart';

@immutable
abstract class SungaiState {}

class SungaiInitial extends SungaiState {
  SungaiInitial();
}

class SungaiBlocState extends SungaiState {
  final Sungai value;

  SungaiBlocState(this.value);
}

class SungaiJarakState extends SungaiState {
  final Sungai value;

  SungaiJarakState(this.value);
}

class SungaiKedalamanState extends SungaiState {
  final Sungai value;

  SungaiKedalamanState(this.value);
}

class SungaiCalculateKetinggianState extends SungaiState {
  final Sungai value;

  SungaiCalculateKetinggianState(this.value);
}
